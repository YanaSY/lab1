#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void	main(void)
{
	double x;
	double f;

	x = 5;
	f = (-1) *(sqrt(x));
	printf("x = %g\nf = %.4g\n", x, f);
	x = 3.0051;
	f = (-1) *(sqrt(x));
	printf("x = %g\nf = %.4g\n", x, f);
	system("pause");
}

